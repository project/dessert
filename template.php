<?php 


//
//  Return a themed breadcrumb trail.
//	Alow you to customize the breadcrumb markup
//

function dessert_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode('<img src="' . base_path() . path_to_theme() . '/images/breadcrumb.gif" alt="&gt;" />', $breadcrumb) .'</div>';
  }
}
