<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
	<title><?php print $head_title; ?></title>
	<?php print $head; ?>
	<?php print $styles; ?>
	<?php print $scripts; ?>
	<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>
<body class="<?php print $body_classes; ?>">
<!-- Body Content -->
<!-- Header -->
<div id="header-wrapper">
	<div id="header" class="clear-block">	
	<div id="header-region">
		<?php print $header; ?>
		<?php if (!empty($search_box)): ?>
			 <div id="search-box">
			 	<?php print $search_box; ?>
			</div>
		<?php endif; ?>
	</div>

	<!-- Logo -->
	<span id="sitename">
	<?php if($logo) : ?>
		<a id="logo" href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" /></a>
	<?php endif; ?>
	<a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" rel="home">
		<?php print $site_name; ?>
		<!-- Site Slogan -->
		<?php if ($site_slogan) : ?>
			<span id="slogan"><?php print $site_slogan; ?></span>
		<?php endif; ?>
		<!-- /Site Slogan -->
	</a>
	</span>
	<!-- /Logo -->
	</div>
</div>
<!-- /Header -->
<!-- Primary + Secondary Navigation -->
<div id="navigation" class="menu <?php if (!empty($primary_links)) { print "withprimary"; } if (!empty($secondary_links)) { print " withsecondary"; }; ?> ">
<?php if (!empty($primary_links)): ?>
	<div id="primary" class="clear-block">
		<?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
	</div>
<?php endif; ?>
<?php if (!empty($secondary_links)): ?>
	<div id="secondary" class="clear-block">
		<?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
	</div>
<?php endif; ?>
</div>
<!-- /Primary + Secondary Navigation -->
<!-- Content + Sidebars -->
<div id="maincontent">
	<?php if($tabs | $title) : ?>
	<div class="node-title<?php if(!empty($tabs)): print " withtabs"; endif; ?> clear-block">
		<?php if(!empty($tabs)): print $tabs; endif; ?>
		<?php if(!empty($title)) : ?><h1><?php print $title; ?></h1><?php endif; ?>
	</div>
	<?php endif; ?>
	<?php if (!empty($messages)): print $messages; endif; ?>
	<?php if (!empty($help)): print $help; endif; ?>
	<?php //Outputs Number of Sidebars Class for Theming
		if (!empty($left) && (!empty($right))):
			$sidebar_state = "two-sidebar"; 
		elseif((!empty($left) && (empty($right))) | (!empty($right) && (empty($left)))):
			$sidebar_state = "one-sidebar";
		elseif(empty($left) && (empty($right))):
			$sidebar_state = "no-sidebar";
		endif;
	?>
	<div id="content" class="<?php print $sidebar_state; ?> clear-block">
		<!-- sidebar-left -->
		<?php if (!empty($left)): ?>
			<div id="sidebar-left">
				  <?php print $left; ?>
			</div> 
		<?php endif; ?>
		<!-- /sidebar-left -->
		<!-- main -->
		<div id="main">
			<?php if (!empty($mission)): ?>
				<div id="mission"><?php print $mission; ?></div>
			<?php endif; ?>
			<?php print $content; ?>
			<?php print $feed_icons; ?>
		</div>
		<!-- /main -->
		<!-- sidebar-right -->
		<?php if (!empty($right)): ?>
			<div id="sidebar-right">
				<?php print $right; ?>
			</div>
		<?php endif; ?>
		<!-- /sidebar-right -->
			<!-- Breadcrumbs -->
			<?php if (!empty($breadcrumb)): ?>
				<div id="breadcrumb"><?php print $breadcrumb; ?></div>
			<?php endif; ?>
		<!-- Breadcrumbs -->
	</div>
</div>
<!-- /Content + Sidebars -->
<!-- Footer -->
<div id="footer-wrapper">
	<div id="footer">
		<?php if($footer): ?>
			<?php print $footer; ?>
		<?php endif; ?>
		<?php if ($footer_message): ?>
			<?php print $footer_message; ?>
		<?php endif; ?>
		<div id="credit">
		<?php print t("Theme by ") . '<a href="' . t('http://www.outcome3.com" title="Vancouver Internet Marketing"') . '>' . t("www.Outcome3.com") . '</a>'; ?>
	</div>
</div>
</div>
<!-- /Footer -->
<!-- /Body Content -->
</body>
<?php print $closure; ?>
</html>
